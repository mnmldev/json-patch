const jsonpatch = require("fast-json-patch")
const { set, remove, clone, equals } = require('svpr')

function makePatches(original, updated) {
  return jsonpatch.compare(original, updated)
}

function makeUpdate(original, updated, { remove_as_null = true } = {}) {
  let patches = makePatches(original, updated)
  let update = {}
  let testsPass = true

  function makePath(slashedPath) {
    return slashedPath.slice(1).replace(/\//gim, ".")
  }

  function removeFromUpdate(path, obj) {
    return remove_as_null ? set(makePath(path), null, obj) : remove(makePath(path), obj)
  }

  for (let i = 0; i < patches.length; i++) {
    let patch = patches[i]
    update = set(makePath(patch.path), {}, update)

    if (patch.op === 'remove') {
      update = removeFromUpdate(patch.path, update)
    } else if (['replace', 'add'].includes(patch.op)) {
      update = set(makePath(patch.path), patch.value, update)
    } else if (['move', 'copy'].includes(patch.op)) {
      let fromValue = get(makePath(patch.from), original)
      if (fromValue !== null) {
        update = set(makePath(patch.path), clone(fromValue), update)
      }
      if (patch.op === 'move') {
        update = removeFromUpdate(patch.from, update)
      }
    } else if (patch.op === 'test') {
      testsPass = testsPass && equals(get(makePath(patch.path), original), patch.value)
    }
  }
  return update
}

function patch(obj = {}, patches = []) {
  return jsonpatch.applyPatch(obj, patches).newDocument
}

module.exports = { makeUpdate, makePatches, patch }