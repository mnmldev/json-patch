const { makeUpdate } = require('./../index')

let person = {
  name: "Gabin",
  address: {
      street: "Sopulikuja 6 E",
      country: "Finland"
  },
  notHere: true
}  

let personUpdated = {
  name: "Gabin Desserprit",
  address: {
      city: "Hetta",
      country: "Finland"
  },
  other: true
} 

let update = makeUpdate(person, personUpdated)
console.log(JSON.stringify(update, null, 2));